<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230203152200 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, num_client_id INT DEFAULT NULL, date_reservation DATE NOT NULL, date_prevue_stockage DATE NOT NULL, nb_jours_de_stockage_prevu INT NOT NULL, quantite INT NOT NULL, etat VARCHAR(255) NOT NULL, INDEX IDX_42C849556B351DFD (num_client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849556B351DFD FOREIGN KEY (num_client_id) REFERENCES `user` (id)');
        $this->addSql('DROP TABLE travee');
        $this->addSql('DROP TABLE reservationStockee');
        $this->addSql('DROP TABLE pile');
        $this->addSql('DROP TABLE bloc');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE travee (numTravee CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, codeBloc CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`) DEFAULT CHARACTER SET utf8mb3 COLLATE `utf8mb3_general_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE reservationStockee (numPile CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, numTravee CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, codeBloc CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, idReservation INT NOT NULL, emplacementDepart INT NOT NULL, quantite INT NOT NULL, dateDebutEffective DATETIME NOT NULL, dateFinEffective DATETIME NOT NULL) DEFAULT CHARACTER SET utf8mb3 COLLATE `utf8mb3_general_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE pile (numPile CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, numTravee CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, codeBloc CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, capacite TINYINT(1) NOT NULL, INDEX numTraveeBloc (numTravee, codeBloc), PRIMARY KEY(numPile, numTravee, codeBloc)) DEFAULT CHARACTER SET utf8mb3 COLLATE `utf8mb3_general_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE bloc (codeBloc CHAR(1) CHARACTER SET utf8mb3 NOT NULL COLLATE `utf8mb3_general_ci`, PRIMARY KEY(codeBloc)) DEFAULT CHARACTER SET utf8mb3 COLLATE `utf8mb3_general_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C849556B351DFD');
        $this->addSql('DROP TABLE reservation');
    }
}
