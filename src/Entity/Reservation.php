<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date_reservation = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date_prevue_stockage = null;

    #[ORM\Column]
    private ?int $nb_jours_de_stockage_prevu = null;

    #[ORM\Column]
    private ?int $quantite = null;

    #[ORM\Column(length: 255)]
    private ?string $etat = null;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    private ?User $num_client = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateReservation(): ?\DateTimeInterface
    {
        return $this->date_reservation;
    }

    public function setDateReservation(\DateTimeInterface $date_reservation): self
    {
        $this->date_reservation = $date_reservation;

        return $this;
    }

    public function getDatePrevueStockage(): ?\DateTimeInterface
    {
        return $this->date_prevue_stockage;
    }

    public function setDatePrevueStockage(\DateTimeInterface $date_prevue_stockage): self
    {
        $this->date_prevue_stockage = $date_prevue_stockage;

        return $this;
    }

    public function getNbJoursDeStockagePrevu(): ?int
    {
        return $this->nb_jours_de_stockage_prevu;
    }

    public function setNbJoursDeStockagePrevu(int $nb_jours_de_stockage_prevu): self
    {
        $this->nb_jours_de_stockage_prevu = $nb_jours_de_stockage_prevu;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getNumClient(): ?User
    {
        return $this->num_client;
    }

    public function setNumClient(?User $num_client): self
    {
        $this->num_client = $num_client;

        return $this;
    }
}
