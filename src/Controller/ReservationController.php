<?php

namespace App\Controller;

use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ReservationController extends AbstractController
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    #[Route('/reservation', name: 'app_reservation')]
    public function index(SerializerInterface $serializer) {

        $reservationsTab = [];

        $idUser = $this->getUser()->getId();

        $reservations = $this->entityManager->getRepository(Reservation::class)->findBy(['num_client' => $idUser]);

        foreach ($reservations as $reservation)
        {
            $jsonContent = $serializer->normalize($reservation);
            $reservationsTab[] = $jsonContent;
        }

        return new JsonResponse($reservationsTab);
    }


    #[Route('/reservation/add', name: 'app_reservation_add')]
    public function store(Request $request, SerializerInterface $serializer) {

        /*$reservation = new Reservation();
        $reservation->setNumClient($this->getUser()->getId());
        $reservation->setEtat("demande");
        $reservation->setDateReservation(new \DateTimeImmutable());
        $reservation->setDatePrevueStockage($request->get('dateDebut'));
        $reservation->setNbJoursDeStockagePrevu($request->get('nbJours'));
        $reservation->setQuantite($request->get('quantite'));*/

        $reservation = new Reservation();
        $reservation->setNumClient($this->getUser());
        $reservation->setEtat("demande");
        $reservation->setDateReservation(new \DateTimeImmutable());
        $reservation->setDatePrevueStockage(new \DateTimeImmutable());
        $reservation->setNbJoursDeStockagePrevu(11);
        $reservation->setQuantite(10);

        $this->entityManager->persist($reservation);
        $this->entityManager->flush($reservation);

        $reservationJson = $serializer->serialize($reservation, 'json');

        dd($reservationJson);


        return new JsonResponse($reservationJson);
    }

    public function deleteReservation() {

    }
}