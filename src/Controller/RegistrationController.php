<?php
/*
namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Json;

class RegistrationController extends AbstractController
{
    #[Route('/register', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $user = new User();
        $requestt = $request->get()

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return new JsonResponse([
                'Resultat' => 'success'
            ]);
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}*/

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\MainAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Validator\Constraints\Json;

class RegistrationController extends AbstractController
{
    public function __construct(private EntityManagerInterface      $entityManager,
                                private UserPasswordHasherInterface $passwordHasher)
    {
    }


    #[Route('/register', name: 'app_register')]
    public function index(Request $request, UserAuthenticatorInterface $userAuthenticator, MainAuthenticator $mainAuthenticator): Response
    {
        $errorsMail = [];
        $errorsPwd = [];

        if ($request->isMethod('POST')) {
            $raisonSociale = $request->get('raisonSociale');
            $type = $request->get('type');
            $login = $request->get('login');
            $password = $request->get('password');

            /*if ($raisonSociale == "") {
                $errorsMail[] = "Votre raison sociale est obligatoire !";
            }
            if (null !== $this->entityManager->getRepository(User::class)->findOneByRaisonSociale($raisonSociale)) {
                $errorsMail[] = "Votre nom est déjà utilisé !";
            }
            if ($password != $cPassword || $cPassword == "") {
                $errorsPwd[] = "Les deux mot de passe ne correspondent pas !";
            }
            if (strlen($password) < 6) {
                $errorsPwd[] = "6 caractères minium requis";
            }*/
            $user = new User();

            $user->setRaisonSociale($raisonSociale);
            $user->setLogin($login);
            $user->setType($type);

            $passHashed = $this->passwordHasher->hashPassword($user, $password);
            $user->setPassword($passHashed);

            $this->entityManager->persist($user);
            $this->entityManager->flush($user);

            $userAuthenticator->authenticateUser($user, $mainAuthenticator, $request);
            return new JsonResponse([
               'Resultat' => 'Success'
            ]);
        }
        return $this->render('registration/register.html.twig');
    }
}

